<?php

class du_trust_tweets {
  
  public function __construct( $count=1, $tweet_query=null ) {
    $this->authentication = du_api_auth::twitter();
    $this->count = $count;
    if ( $tweet_query ) 
      $this->tweet_query = $tweet_query;
  }
  
  public function cache_tweets( $feed_type, $interval=600 ){
    switch ( $feed_type ) {
      case 'favorites':
        $feed_type_link = 'favorites/list';
        break;
      
      case 'user':
        $feed_type_link = 'statuses/user_timeline';
        break;

      default:
        $feed_type_link = 'favorites/list';  
        break;
    }

    $username = $this->authentication->username;
    $feed = "https://api.twitter.com/1.1/"
      . $feed_type_link 
      . ".json?screen_name={$username}&count={$this->count}" . $this->tweet_query;
    $cache_file = dirname(__FILE__).'/cache/'.'twitter-' . $feed_type . '-cache.cache';
    $modified = filemtime( $cache_file );
    $now = time();

    if ( !$modified || ( ( $now - $modified ) > $interval ) ) {
      $bearer = $this->authentication->bearer_token;
      $context = stream_context_create(array(
        'http' => array(
          'method'=>'GET',
          'header'=>"Authorization: Bearer " . $bearer
          )
      ));
      $json = file_get_contents( $feed, false, $context );
      
      if ( $json ) {
        $cache_static = fopen( $cache_file, 'w+' );
        fwrite( $cache_static, $json );
        fclose( $cache_static );
      }
    }

    //header( 'Cache-Control: no-cache, must-revalidate' );
    //header( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
    //header( 'Content-type: application/json' );
     
    $this->tweets = file_get_contents( $cache_file );
    $this->tweets = json_decode( $this->tweets, false );
  }

  public function audit_tweets( $hashtags, $feed_type='favorites' ){
    
    /* First let's make sure to update tweet cache */
    $this->cache_tweets( $feed_type );

    /* Set hashtags to search for */
    $this->hashtags = $hashtags;

    /**
     * Get stored existing tweet ids
     */
    $existing_ids = array();

    foreach ( $this->tweets as $tweet ) {
      /**
       * Grab information from existing tweet posts to test
       * against
       */
      $social_posts = new WP_Query( 'post_type=social_posts&posts_per_page=99999&post_status=publish' );
      while ( $social_posts->have_posts() ) : $social_posts->the_post();
        $tweet_id = get_post_meta( get_the_ID(), 'tweet_id', true );
        array_push( $existing_ids, $tweet_id );

        /**
         * If the current reply_to_status_id is equivalent to the current
         * post tweet_id, let's note this posts id as the parent
         */
        if ( $tweet->in_reply_to_status_id_str == $tweet_id )
          $parent = get_the_ID();

      endwhile;

      /* If this tweet exists, don't add it again */
      if ( in_array( $tweet->id, $existing_ids )  ) {
        continue;
      }

      /**
       * Check for attached images to use
       * 
       */
      $featured_image = false;
      if ( isset( $tweet->entities->media ) ) :
        foreach ($tweet->entities->media as $attachment ) {
          if ( $attachment->type == "photo" ) :
            $featured_image = $attachment->media_url;
          endif;
        }
      endif;

      /**
       * If hashtags are present, let's generate an array for
       * them to pass to the add_tweet_post function.
       */
      if ( isset( $tweet->entities->hashtags ) ) :
        $hashtags_used = array();
        foreach ( $tweet->entities->hashtags as $tag ) {
          array_push( $hashtags_used, $tag->text );
        }
      endif;

      /**
       * Let's clean this thing up
       */
      $tweet_id             = $tweet->id;
      $username             = $tweet->user->screen_name;
      $content              = $tweet->text;
      $profile_photo        = $tweet->user->profile_image_url;
      $favorite_count       = $tweet->favourites_count;
      $date_posted          = $tweet->created_at;
      $image_url            = ( !empty($featured_image) ? $featured_image : false );


      /**
       * Pass the data to the add_tweet_post function
       */
      $this->add_tweet_post( $username, $content, $image_url, $profile_photo, $date_posted, $tweet_id, $parent, $hashtags_used, $favorites_count );
    
    }

  }

  public function add_tweet_post( $username, $content, $image_url=false, $profile_photo, $date_posted, $tweet_id, $parent=null, $hashtags_used=null, $favorites_count=0 ){

    /**
     * If hashtags are present, lets test them against our
     * set of hashtags
     */
    if ( $hashtags_used ) {
      $tax_hashes = array();
      foreach ( $hashtags_used as $hash ) {
        if ( strtolower( $hash ) == $this->hashtags ) {
          $new_tweet = array(
            'post_title'      => $username,
            'post_type'       => 'social_posts',
            'post_status'     => 'publish',
            'post_date'       => date( 'Y-m-d H:i:s', strtotime( $date_posted ) )
          );

          /**
           * If this is a reply, make it a child of original tweet
           */
          if ( !empty( $parent ) ) {
            $new_tweet[ 'post_parent' ] = $parent;
          }

          /**
           * Generate post for this tweet
           */
          $new_tweet_id = wp_insert_post( $new_tweet );
          add_post_meta( $new_tweet_id, 'tweet_id', $tweet_id );
          add_post_meta( $new_tweet_id, 'profile_photo', $profile_photo );
          add_post_meta( $new_tweet_id, 'tweet_text', $content );
          add_post_meta( $new_tweet_id, 'favorite_count', $favorites_count );
          add_post_meta( $new_tweet_id, 'network', 'twitter' );
          if ( $image_url ) :
            add_post_meta( $new_tweet_id, 'tweet_image', $image_url );
          endif; 

          array_push( $tax_hashes, $hash );
        }
      }
      wp_set_post_terms( $new_tweet_id, $tax_hashes, 'du_hashtags', false );
    }

  }

  public function show_tweets( $template='default', $feed_type='user' ) {
    
    $this->cache_tweets( $feed_type );

    if ( !empty( $this->tweets ) ) {
      global $tweet;
      $current_tweet = 1;
      foreach( $this->tweets as $tweet ) {
        if ( $current_tweet > $this->count )
          break;
        # Access as an object
        $tweet->created_at = date( "l M d", strtotime( $tweet->created_at ) );
        $tweet->text = preg_replace( "/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $tweet->text );
        $tweet->text = preg_replace( "/@([A-Za-z0-9\/\._]*)/", "<a target=\"_blank\" href=\"http://www.twitter.com/$1\">@$1</a>", $tweet->text );
        $tweet->text = preg_replace( "/#([A-Za-z0-9\/\._]*)/", "<a target=\"_blank\" href=\"http://twitter.com/search?q=$1\">#$1</a>", $tweet->text );
        $tweet->current_tweet = $current_tweet;
        include( get_stylesheet_directory() . '/templates/tweets/' . $template . '.inc' );
        $current_tweet++;
      }
      /**
       * Let's destroy the global variable once we're done
       * passing it to the template
       */
      unset( $tweet );
    }
  }
}
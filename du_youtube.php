<?php
/**
 * Pull YouTube videos from API
 * Arguments to be passed as an array!
 *
 * @return void
 * @author mohammad@duable.com
 * @var id        Video, user, or playlist id
 * @var count     Max results
 * @var template  Tamplate to return
 * @var feed_type Type of feed to return from API
 * 
 **/
function du_youtube_videos( $args = array() ) {
  if ( empty( $args[ 'id' ] ) )
    return;

  if ( empty( $args[ 'count' ] ) )
    $args[ 'count' ] = '1';

  if ( empty( $args[ 'template' ] ) )
    $args[ 'template' ] = 'default';

  if ( empty( $args[ 'feed_type' ] ) )
    $args[ 'feed_type' ] = 'playlistItems';
  
  $yt = new du_trust_youtube( $args[ 'count' ], $args[ 'id' ] );
  $yt->show_videos( $args[ 'template' ], $args[ 'feed_type' ] );
}

class du_trust_youtube {
  
  public function __construct( $count=1, $id ) {
    $this->count = $count;
    $this->id = $id;
    $this->api_key = du_api_auth::youtube();
  }
  
  public function cache_video( $feed_type, $interval=43200 ){
    switch ( $feed_type ) {
      case 'playlistItems':
        $this->feed_type_link = 'playlistItems';
        break;

      default:
        $this->feed_type_link = 'playlistItems';  
        break;
    }
    $feed = 'https://www.googleapis.com/youtube/v3/' . $this->feed_type_link . '?';   
    $options = array( 
      "part" => "contentDetails",
      "playlistId" => $this->id,
      "key" => $this->api_key
    );
    $feed .= http_build_query( $options, '', '&' );

    $cache_file = dirname(__FILE__).'/cache/'.'yt-' . $feed_type . '-cache.cache';
    @$modified = filemtime( $cache_file );
    $now = time();

    if ( !$modified || ( ( $now - $modified ) > $interval ) ) {
      $xml_file = file_get_contents( $feed, false );
      
      if ( $xml_file ) {
        $cache_static = fopen( $cache_file, 'w+' );
        fwrite( $cache_static, $xml_file );
        fclose( $cache_static );
      }
    }

    $this->videos = file_get_contents( $cache_file );
    $this->videos = json_decode( $this->videos );
  }

  public function show_videos( $template='default', $feed_type='playlistItems' ) {
    $this->cache_video( $feed_type );
    $videos = new stdClass();
    $count = 0;
    foreach ( $this->videos->items as $video ) {
      $video->link = 'https://www.youtube.com/?v=' . $video->contentDetails->videoId;
      set_query_var( 'count', $count );
      set_query_var( 'video', $video );
      get_template_part( 'templates/youtube', $template );
    }
  }

}
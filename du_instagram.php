<?php
class du_trust_instagram {

  function __construct( $template, $client_id, $token, $count=5 ){
    $this->template = $template;
    $this->user_id = $client_id;
    $this->token = $token;
    $this->count = $count;
  }

  function fetch( $url ) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }

  function grab_latest() {
    $result = $this->fetch("https://api.instagram.com/v1/users/" . $this->user_id . "/media/recent?access_token=" . $this->token . "&count=" . $this->count );
    $result = json_decode( $result );
    $ig_feed = new stdClass();
    global $gram;
    $current_post = 1;
    foreach ( $result->data as $gram ) {
      $gram->current_gram = $current_post;
      include( get_stylesheet_directory() . '/templates/instagram/' . $this->template . '.inc' );
      $current_post++;
    }

  }

  public function add_ig_post( $username, $content, $image_url=false, $profile_photo, $date_posted, $gram_id ){
    $new_ig = array(
      'post_title'       => $username,
      'post_type'       => 'social_posts',
      'post_status'     => 'pending',
    );
    $new_ig_id = wp_insert_post( $new_ig );
    add_post_meta( $new_ig_id, 'gram_id', $gram_id );
    add_post_meta( $new_ig_id, 'profile_photo', $profile_photo );
    add_post_meta( $new_ig_id, 'tweet_text', $content );
    add_post_meta( $new_ig_id, 'network', 'ig' );
    if ( $image_url ) :
      add_post_meta( $new_ig_id, 'tweet_image', $image_url );
    endif; 
  }

}

function du_instagram_recent( $template='default', $count=10 ) {
  $authentication = du_api_auth::instagram();
  $client_id = $authentication->client_id;
  $token = $authentication->token;
  $instagram = new du_trust_instagram( $template, $client_id, $token, $count );
  $instagram->grab_latest();
}